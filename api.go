package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

var todos map[int]TodoItem

func getAllTodo(c *gin.Context) {
	c.JSON(200, queryAll())
}

func getTodoByID(c *gin.Context) {
	todoID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	results, err := queryByID(todoID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if len(results) == 0 {
		c.JSON(http.StatusNotFound, gin.H{"result": "Todo ID: " + string(todoID) + " not found"})
		return
	}

	c.JSON(http.StatusOK, results[0])
}

func createTodo(c *gin.Context) {
	var todo TodoItem
	if err := c.ShouldBindJSON(&todo); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	Save(todo)
	c.JSON(http.StatusOK, gin.H{
		"message": "Created",
	})
}

func deleteTodo(c *gin.Context) {
	todoID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	deleteByID(todoID)

	c.JSON(http.StatusNoContent, gin.H{})
}

func main() {
	r := gin.Default()
	todos = map[int]TodoItem{
		1: TodoItem{
			ID:     1,
			Title:  "Hiking",
			Status: "Pending",
		},
		2: TodoItem{
			ID:     2,
			Title:  "Shopping",
			Status: "Pending",
		},
	}
	r.GET("/todos", getAllTodo)
	r.GET("/todos/:id", getTodoByID)

	r.POST("/todos", createTodo)

	r.DELETE("/todos/:id", deleteTodo)
	ConnectDB(os.Getenv("DATABASE_URL"))
	fmt.Println("Starting...")
	log.Fatal(r.Run(":1234"))
	fmt.Println("Stop...")
}

type TodoItem struct {
	ID     int    `json:"id"`
	Title  string `json:"title"`
	Status string `json:"status"`
}

var database *sql.DB

func ConnectDB(databaseURL string) {
	db, err := sql.Open("postgres", databaseURL)
	database = db
	if err != nil {
		log.Fatal(err)
	}
	//defer db.Close()

	fmt.Println("Connected Database")
	//database = db
	createDB := `
			CREATE TABLE IF NOT EXISTS todos(
				id SERIAL PRIMARY KEY,
				title TEXT,
				status TEXT
			)
		`

	_, err = db.Exec(createDB)
	if err != nil {
		log.Fatal("Create Table Fail", err)
	}
}

func Save(t TodoItem) {
	insertString := `INSERT INTO todos(title, status) VALUES($1, $2) RETURNING id`
	var id int
	err := database.QueryRow(insertString, t.Title, t.Status).Scan(&id)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("ID = %d was created\n", id)
}

func deleteByID(id int) {
	queryString := `DELETE FROM todos WHERE id=$1 RETURNING id`
	var deletedID int
	err := database.QueryRow(queryString, id).Scan(&deletedID)
	if err != nil {
		log.Fatal(err)
	}

}

func queryByID(id int) ([]TodoItem, error) {

	queryString := `SELECT * FROM todos WHERE id=$1`
	rows, err := database.Query(queryString, id)
	if err != nil {
		log.Fatal(err)
	}
	results := []TodoItem{}
	result := TodoItem{}
	if rows.Next() {
		err = rows.Scan(&result.ID, &result.Title, &result.Status)
		if err != nil {
			log.Fatal(err)
		} else {
			results = append(results, result)
		}
	}
	return results, err
}

func queryAll() []TodoItem {
	var results []TodoItem
	queryString := `SELECT * FROM todos`
	rows, err := database.Query(queryString)
	if err != nil {
		log.Fatal(err)
	}

	for rows.Next() {
		t := TodoItem{}
		err = rows.Scan(&t.ID, &t.Title, &t.Status)
		if err != nil {
			log.Fatal(err)
		} else {
			results = append(results, t)
		}
	}

	return results
}
